package com.example.piyawatbasickotlin.kotlinUnit1

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.piyawatbasickotlin.R

private const val TAG = "DebugTest"

class DebugTest : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debug_test)
        supportActionBar?.title = "Debug Test"
        val actionBar: ActionBar? = supportActionBar
        val colorDrawable = ColorDrawable(Color.parseColor("#FF3700B3"))
        actionBar?.setBackgroundDrawable(colorDrawable)
        Log.d(TAG, "this is where the app crashed before")
        val helloTextView: TextView = findViewById(R.id.division_textview)
        Log.d(TAG, "this should be logged if the bug is fixed")
        helloTextView.text = "Hello, debugging!"
        logging()
        division()

    }
    //test Log class
    fun logging() {
        Log.e(TAG, "ERROR: a serious error like an app crash")
        Log.w(TAG, "WARN: warns about the potential for serious errors")
        Log.i(TAG, "INFO: reporting technical information, such as an operation succeeding")
        Log.d(TAG, "DEBUG: reporting technical information useful for debugging")
        Log.v(TAG, "VERBOSE: more verbose than DEBUG logs")
    }
    //create function division to see how LogCat will respone when Code is Bug Ps.fixed bug
    fun division() {
        val numerator = 60
        var denominator = 4
        repeat(4) {
            findViewById<TextView>(R.id.division_textview).text = "${numerator / denominator}"
            Thread.sleep(3)
            Log.v(TAG, "${numerator / denominator}")
            //change text on screen
            findViewById<TextView>(R.id.division_textview).text = "${numerator / denominator}"
            denominator--
        }
    }
}