package com.example.piyawatbasickotlin.kotlinUnit1

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.piyawatbasickotlin.R
import com.example.piyawatbasickotlin.kotlinUnit2.Unit2Menu

class Unit : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unit)
        supportActionBar?.title = "Units"
        val actionBar: ActionBar? = supportActionBar
        val colorDrawable = ColorDrawable(Color.parseColor("#8400ff"))
        actionBar?.setBackgroundDrawable(colorDrawable)
        val unitOneClick = findViewById<Button>(R.id.Unit1)
        val unitTwoClick = findViewById<Button>(R.id.Unit2)

        unitOneClick?.setOnClickListener{
            val clickedUnitOne = Intent(this, MainActivity::class.java)
            startActivity(clickedUnitOne)
        }

        unitTwoClick?.setOnClickListener{
            val clickedUnitTwo = Intent(this, Unit2Menu::class.java)
            startActivity(clickedUnitTwo)
        }
    }
}