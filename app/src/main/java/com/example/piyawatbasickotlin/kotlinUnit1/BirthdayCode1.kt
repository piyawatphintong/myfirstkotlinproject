package com.example.piyawatbasickotlin.kotlinUnit1

fun main() {
    val border = "`-._,-'"
    val repeatTime = 4
    val age = 24
    val layers = 5
    printBorder(border,repeatTime)
    println("    Happy Birthday, Jhansi!")
    printBorder(border,repeatTime)
    println()
    printCakeCandels(age)
    printCakeTop(age)
    printCakeBottom(age,layers)
}
fun printCakeTop(age:Int){
    repeat(age + 2) {
        print("=")
    }
    println()
}

fun printCakeCandels(age:Int){
    print(" ")//create space so the candel is not on the edge of cake
    repeat(age){
        print(",")
    }
    println()

    print(" ")//create space so the candel is not on the edge of cake
    repeat(age){
        print("|")
    }
    println()
}

fun printCakeBottom(age: Int,layers:Int){
    repeat(layers){
        repeat(age + 2) {
            print("@")
        }
        println()
    }

}

fun printBorder(border: String,repeatTime:Int) {
    repeat(repeatTime) {
        print(border)
    }
    println()
}