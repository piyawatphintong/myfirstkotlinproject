package com.example.piyawatbasickotlin.kotlinUnit1

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.piyawatbasickotlin.R


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = "Unit1"
        //change appbar text and color
        val actionBar: ActionBar? = supportActionBar
        val colorDrawable = ColorDrawable(Color.parseColor("#FF018786"))
        actionBar?.setBackgroundDrawable(colorDrawable)

        //fineVieById for use on onclick
        val birthDayClick = findViewById<Button>(R.id.BirthDayCard)
        val rollDiceClick = findViewById<Button>(R.id.RollDice)
        val debugClick = findViewById<Button>(R.id.DebugTestButton)
        val lemonadeClick = findViewById<Button>(R.id.lemonadeButton)

        //onClickListener of Birth Day button
        birthDayClick.setOnClickListener {
            val clickedBirthDay = Intent(this, BirthDayCard::class.java)
            startActivity(clickedBirthDay)
        }

        //onClickListener of Roll Dice button
        rollDiceClick.setOnClickListener {
            val clickedRollDice = Intent(this, RollTheDIce::class.java)
            startActivity(clickedRollDice)
        }

        //onClickListener of Debug button
        debugClick.setOnClickListener {
            val clickedDebug = Intent(this, DebugTest::class.java)
            startActivity(clickedDebug)
        }

        //onClickListener of Lemonade button
        lemonadeClick.setOnClickListener {
            val clickedLemonade = Intent(this, lemonnade::class.java)
            startActivity(clickedLemonade)
        }
    }
}