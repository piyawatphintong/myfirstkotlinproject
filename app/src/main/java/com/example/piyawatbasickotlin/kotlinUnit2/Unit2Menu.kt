package com.example.piyawatbasickotlin.kotlinUnit2

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.example.piyawatbasickotlin.R

class Unit2Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unit2_menu)
        supportActionBar?.title = "Unit2"
        val actionBar: ActionBar? = supportActionBar
        val colorDrawable = ColorDrawable(Color.parseColor("#4287f5"))
        val TipCalClick = findViewById<Button>(R.id.TipCalculatorButton)

        TipCalClick .setOnClickListener {
            val clickedTipCal  = Intent(this, TipCalculator::class.java)
            startActivity(clickedTipCal)
        }
    }
}